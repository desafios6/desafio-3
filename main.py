array = [-1, -2, 0]
product = [] #guarda a combinação que gera o maior produto
aux_negative = [] #guarda todos os números negativos do array

#caso existam apenas dois números, fazer deles o maior produto
if len(array) == 2:
    for j in range(len(array)):
        array[j] = int(array[j])
    product = array


else:
    for i in range(len(array)): #percorre o vetor, separando positivos e negativos
        if array[i] > 1:
            product.append(array[i])

        elif array[i] <= 0:
            aux_negative.append(array[i])

    if len(aux_negative) % 2 == 0: #se houver uma quantidade par de negativos, adicionar todos ao vetor resultado
        for num in aux_negative:
            product.append(num)

    else:
        while len(aux_negative) > 1: #adicionar os pares de negativos que geram o maior produto
            product.append(min(aux_negative))
            aux_negative.remove(min(aux_negative))

print("a sequência que gera o maior produto é: {}".format(product))
